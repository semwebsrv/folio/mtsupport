package mtsupport.controllers;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import io.micronaut.http.client.annotation.*;
import jakarta.inject.Inject;
import static org.junit.jupiter.api.Assertions.*;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.HttpRequest;
import semweb.mtsupport.dto.OkapiTenantCreateRequest;

// https://dzone.com/articles/micronaut-tutorial-server-application

@MicronautTest
public class OkapiControlPlaneControllerTest {

    @Inject
    @Client("/")
    HttpClient client;

    @Test
    public void testOkapiTenantCreate() throws Exception {
        OkapiTenantCreateRequest otcr = new OkapiTenantCreateRequest();
        HttpRequest request = HttpRequest.POST("/_/tenant",otcr)
                                 .header("X-OKAPI-TENANT","TestTenant1");
        assertEquals(HttpStatus.CREATED, client.toBlocking().exchange(request).status());
    }

    @Test
    public void testOkapiTenantDelete() throws Exception {
        HttpRequest request = HttpRequest.DELETE("/_/tenant")
                                 .header("X-OKAPI-TENANT","TestTenant2");
        assertEquals(HttpStatus.NO_CONTENT, client.toBlocking().exchange(request).status());
    }

    @Test
    public void testOkapiTenantDisable() throws Exception {
        HttpRequest request = HttpRequest.POST("/_/tenant/disable","")
                                 .header("X-OKAPI-TENANT","TestTenant3");
        assertEquals(HttpStatus.NO_CONTENT, client.toBlocking().exchange(request).status());
    }

}
