package semweb.mtsupport.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jakarta.inject.Singleton;

/**
 * @See https://github.com/tqad/micronaut-data-jpa-multi-tenant
 */
@Singleton
public class SchemaManagementService {

  private static Logger log = LoggerFactory.getLogger(SchemaManagementService.class);

  public void enableModuleForTenant(String tenant, String module_id) {
    log.debug("SchemaManagementService::enableModuleForTenant(%s,%s)".formatted(tenant,module_id));
  }
}
