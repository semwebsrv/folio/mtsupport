package semweb.mtsupport.controllers;

import javax.validation.Valid;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Delete;
import jakarta.inject.Inject;
import io.micronaut.context.event.ApplicationEventPublisher;
import semweb.mtsupport.events.TenantCreateRequestEvent;
import semweb.mtsupport.dto.OkapiTenantCreateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.micronaut.http.annotation.Header;

@Controller("/_/tenant")
public class OkapiControlPlane {

  private static Logger log = LoggerFactory.getLogger(OkapiControlPlane.class);

  @Inject ApplicationEventPublisher eventPublisher;

  // POST or DELETE to /_ is enable or delete tenant - enable does a range of things
  @Post()
  public HttpResponse<?> enableTenant(@Body @Valid OkapiTenantCreateRequest tenant_create_request,
                                      @Header("X-OKAPI-TENANT") String tenant) {
    log.debug("Incoming tenant: %s".formatted(tenant));
    TenantCreateRequestEvent tcre = new TenantCreateRequestEvent(tenant);
    log.debug("OkapiControlPlane::enableTenant(%s)".formatted(tcre.toString()));
    eventPublisher.publishEvent(tcre);
    return HttpResponse.status(HttpStatus.CREATED).body("Tenant Created!");
  }

  @Delete()
  public HttpResponse<?> deleteTenant() {
    return HttpResponse.status(HttpStatus.NO_CONTENT);
  }

  // /tenant/disable
  @Post(uri="/disable")
  public HttpResponse<?> disableTenant() {
    return HttpResponse.status(HttpStatus.NO_CONTENT);
  }



}
