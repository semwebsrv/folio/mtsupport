package semweb.mtsupport.events;

public class TenantCreateRequestEvent {

  public TenantCreateRequestEvent(String tenant) {
    this.tenant = tenant;
  }

  public String tenant;


  public String toString() {
    return "TenantCreateRequestEvent(%s)".formatted(tenant);
  }
}
