package semweb.mtsupport.dto;

// https://dzone.com/articles/micronaut-tutorial-server-application
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public class OkapiTenantCreateRequest {

  public String toString() {
    return "OkapiTenantCreateRequest()";
  }
}
