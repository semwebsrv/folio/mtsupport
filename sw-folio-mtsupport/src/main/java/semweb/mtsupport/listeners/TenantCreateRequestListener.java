package semweb.mtsupport.listeners;

import jakarta.inject.Singleton;
import io.micronaut.context.event.ApplicationEventListener;
import semweb.mtsupport.events.TenantCreateRequestEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @See https://docs.micronaut.io/1.1.0.M2/guide/index.html#contextEvents
 */
@Singleton
public class TenantCreateRequestListener implements ApplicationEventListener<TenantCreateRequestEvent> {

  private static Logger log = LoggerFactory.getLogger(TenantCreateRequestListener.class);

  @Override
  public void onApplicationEvent(TenantCreateRequestEvent event) {
    log.debug("TenantCreateRequestListener::onApplicationEvent()");
  }
}
